$(".userpass").on("click",function() {//Login
	var user = $(".userform").val();
	var pass = $(".passform").val();
	$.ajax({
			   type: "POST",
			   url: "index.php/C_welcome/getlogin",
			   data:
			   {
			   "user":user,
			   "pass":pass
			   },
			   cache: "false",
			   success: function(result){
						if ( result == "TRUE" ){
							window.location.href = '/labocall/index.php/C_users/index/'+user
						}
						else{
							return false;
						}

			   }
			 });
});
$("#savedata").on("click",function() {//Guardar cambios en tu perfil
	var user = $("#user").val();
	var nomuser = $("#nomuser").val();
	var suruser = $("#suruser").val();
	var tlfuser = $("#tlfuser").val();
	var extuser = $("#extuser").val();
	var emailuser = $("#emailuser").val();
	var ultima_sesion = $("#ultima_sesion").val();
	var imguser = $("#imguser").val();
	$.ajax({
			   type: "POST",
			   url: "C_profile/changeprofile",
			   data:
			   {
			   "user":user,
			   "nomuser":nomuser,
			   "suruser":suruser,
			   "tlfuser":tlfuser,
			   "extuser":extuser,
			   "emailuser":emailuser,
			   "ultima_sesion":ultima_sesion,
			   "imguser":imguser,
			   },
			   cache: "false",
			   success: function(result){
			   }
			 });
});
$("#insertcli").on("click",function() {//Dar alta cliente
	var cli_cod = $("#cli_cod").val();
	var cli_nom = $("#cli_nom").val();
	var cli_sur = $("#cli_sur").val();
	var car_tlf = $("#car_tlf").val();
	var car_ext = $("#car_ext").val();
	var car_buzon = $("#car_buzon").val();
	var cli_email = $("#cli_email").val();
	var cli_fecha_alta = $("#cli_fecha_alta").val();
	$.ajax({
			   type: "POST",
			   url: "insertcli",
			   data:
			   {
			   "cli_cod":cli_cod,
			   "cli_nom":cli_nom,
			   "cli_sur":cli_sur,
			   "car_tlf":car_tlf,
			   "car_ext":car_ext,
			   "car_buzon":car_buzon,
			   "cli_email":cli_email,
			   "cli_fecha_alta":cli_fecha_alta,
			   },
			   cache: "false",
			   success: function(result){
				var json = $.parseJSON(result);
				console.log(json);
				 $("#searchcontent").html("<tr><td align='center' id = 'cli_cod'>"+json['cli_cod']+"</td><td align='center' id = 'cli_nom'>"+json['cli_nom']+"</td><td align='center' id = 'cli_sur'>"+json['cli_sur']+"</td><td align='center' id = 'cli_email'>"+json['cli_email']+"</td><td align='center' id = 'car_ext'>"+json['car_ext']+"</td><td align='center' id = 'cli_fecha_alta'>"+json['cli_fecha_alta']+"</td><td align='center' id = 'car_tlf'>"+json['car_tlf']+"</td><td align='center'>"+json['car_buzon']+"</td><td><button id = 'editcustom'>Editar</button></td></tr>");
				$("#dialog").show();
				}
			 });
});
$("#searchcli").on("click",function() {// Buscar Datos de un Cliente
	var cli_cod = $("#cli_cod").val();
	var cli_nom = $("#cli_nom").val();
	var cli_sur = $("#cli_sur").val();
	var car_tlf = $("#car_tlf").val();
	var car_ext = $("#car_ext").val();
	var car_buzon = $("#car_buzon").val();
	var cli_email = $("#cli_email").val();
	var cli_fecha_alta = $("#cli_fecha_alta").val();
	var cli_activo = $("#cli_activo").val();
	$.ajax({
			   type: "POST",
			   url: "buscarcli",
			   data:
			   {
			   "cli_cod":cli_cod,
			   "cli_nom":cli_nom,
			   "cli_sur":cli_sur,
			   "car_tlf":car_tlf,
			   "car_ext":car_ext,
			   "car_buzon":car_buzon,
			   "cli_email":cli_email,
			   "cli_fecha_alta":cli_fecha_alta,
			   "cli_activo":cli_activo,
			   },
			   cache: "false",
			   success: function(result){
				   var json = $.parseJSON(result);
				   console.log(json);
				   $("#searchcontent").html("<tr><td class='col-xs-1'align='center' ><input type='text' class='cli_cod' size='1' value='"+json['cli_cod']+"'/></div></td><td align='center' class='col-xs-1'><label><input size='4' class='cli_nom' type='text' value='"+json['cli_nom']+"'/></label></td><td align='center' class='col-xs-2'><label><input class='cli_sur' type='text' value='"+json['cli_sur']+"'/></label></td><td align='center' class = 'col-xs-2'><label><input class='cli_email' type='text' value='"+json['cli_email']+"'/></label></td><td align='center' class = 'col-xs-1'><label><input class='car_ext'size ='2' type='text' value='"+json['car_ext']+"'/></label></td><td align='center' class = 'cli_fecha_alta'><label><input size='6' class='cli_fecha_alta' type='text' value='"+json['cli_fecha_alta']+"'/></label></td><td align='center' class = 'car_tlf'><label><input size='7' class='car_tlf' type='text' value='"+json['car_tlf']+"'/></label></td><td align='center' ><label><input size='1' class='car_buzon' type='text' value='"+json['car_buzon']+"'/></label></td><td align='center'><button type='button' id='1' class='1' data-toggle='modal' data-target='#myModal'>Abrir</button></td></tr>");
					 	$("input#cli_cododc").val(json['cli_cod']);
					 	$("input#cli_nomodc").val(json['cli_nom']);
					 	$("input#cli_surodc").val(json['cli_sur']);
						$("input#car_tlfodc").val(json['car_tlf']);
						$("input#car_extodc").val(json['car_ext']);
					 	$("input#car_buzonodc").val(json['car_buzon']);
					 	$("input#cli_emailodc").val(json['cli_email']);
					 	$("input#cli_fecha_altaodc").val(json['cli_fecha_alta']);
					}
			 });
});
$( document ).ready(function() {// Buscar user cuando carga la página
			var user = $('span#username').text();
		$.ajax({
				   type: "POST",
				   url: "C_profile/buscaruser",
				   data:
				   {
						 "user":user,
				   },
				   cache: "false",
				   success: function(result){
						  var json = $.parseJSON(result);
							console.log(json);
							$("#user").val(json['user']);
							$("#nomuser").val(json['nomuser']);
							$("#suruser").val(json['suruser']);
							$("#tlfuser").val(json['tlfuser']);
							$("#emailuser").val(json['emailuser']);
							$("#extuser").val(json['extuser']);
							$("#ultima_sesion").val(json['ultima_sesion']);
							//$("#imguser").val(json['imguser']);
						}
				 });
	});
	$("#edicustom").on("click",function() {//Editar un cliente
		var cli_cod = $("#cli_cododc").val();
		var cli_nom = $("#cli_nomodc").val();
		var cli_sur = $("#cli_surodc").val();
		var car_tlf = $("#car_tlfodc").val();
		var car_ext = $("#car_extodc").val();
		var car_buzon = $("#car_buzonodc").val();
		var cli_email = $("#cli_emailodc").val();
		var cli_fecha_alta = $("#cli_fecha_altaodc").val();
		var cli_activo = $("#cli_activoodc").val();
		$.ajax({
				   type: "POST",
				   url: "editcli",
				   data:
				   {
				   "cli_cod":cli_cod,
				   "cli_nom":cli_nom,
				   "cli_sur":cli_sur,
				   "car_tlf":car_tlf,
				   "car_ext":car_ext,
				   "car_buzon":car_buzon,
				   "cli_email":cli_email,
				   "cli_fecha_alta":cli_fecha_alta,
				   "cli_activo":cli_activo,
				   },
				   cache: "false",
				   success: function(result){
						}
				 });
	});
