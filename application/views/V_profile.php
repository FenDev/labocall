<?php include('V_header.php') ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Perfil</a></div>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
        <div class= "col-lg-6">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Técnico</h5>
          </div>
          <div class="widget-content nopadding">
            <form action="#" method="get" class="form-horizontal">
              <div id = 'opencustomdetailcontent'>
                <!---Usuario -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="user" type="text" class="form-control" name="user" placeholder="Usuario">
                </div>
                <!---Nombre -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="nomuser" type="text" class="form-control" name="nomuser" placeholder="Nombre">
                <!---Apellidos -->
                <div class="input-group col-xs-">
                </div>
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="suruser" type="text" class="form-control" name="suruser" placeholder="Apellido">
                </div>
                <!---Email -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-inbox"></i></span>
                <input id="emailuser" type="text" class="form-control" name="emailuser" placeholder="Email">
                </div>
                <!---Fecha de alta -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                <input id="ultima_sesion" type="text" class="form-control" name="ultima_sesion" placeholder="Fecha de alta">
                </div>
                <!---TLF -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                <input id="tlfuser" type="text" class="form-control" name="tlfuser" placeholder="Tlf">
                </div>
                <!---Extensión -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i></span>
                <input id="extuser" type="text" class="form-control" name="extuser" placeholder="Extension">
                </div>
              </div>
        			  <div class="widget-content nopadding">
                    <form action="#" method="get" class="form-horizontal">
                      <div class="control-group">
                        <label class="control-label">Subir foto</label>
                        <div class="controls">
                          <input id="imgcli" type="file" />
                        </div>
                      </div>
                    </form>
                  </div>
              <div class="form-actions">
                <button type="submit" id="savedata" class="btn btn-success">Guardar</button>
                <button id="searchuser">Visualizar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
        <div class = "col-lg-6">
          <img width="600" height="400" class = "img-rounded">
        </div>
      </div>
    </div><hr>
    </div>

  </div>
<?php include('V_footer.php') ?>
