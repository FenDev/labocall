<?php include('V_header.php') ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="Igniter/C_users" class="tip-bottom">Clientes</a> <a href="#" class="current">Datos Personales</a> </div>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Búsqueda de Clientes</h5>
          </div>
    			<div class = 'col-sm-4'>
            <h5>Datos personales</h5>
                <!---Código Cliente -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="cli_cod" type="text" class="form-control" name="cli_cod" placeholder="Código">
                </div>
                <!---Nombre -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="cli_nom" type="text" class="form-control" name="cli_nom" placeholder="Nombre">
                </div>
                <!---Apellidos -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="cli_sur" type="text" class="form-control" name="cli_sur" placeholder="Apellidos">
                </div>
                <!---Email -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-inbox"></i></span>
                <input id="cli_email" type="text" class="form-control" name="cli_email" placeholder="Email">
                </div>
          </div>
          <div class = 'col-sm-4'>
          <h5>Datos de Facturación<h5>
                <!---Fecha de alta -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                <input id="cli_fecha_alta" type="text" class="form-control" name="cli_fecha_alta" placeholder="Fecha de alta">
                </div>
                <!---TLF -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                <input id="car_tlf" type="text" class="form-control" name="car_tlf" placeholder="Tlf">
                </div>
                <!---Extensión -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i></span>
                <input id="car_ext" type="text" class="form-control" name="car_ext" placeholder="Extension">
                </div>
                <!---Buzón -->
                <div class="input-group col-xs-">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input id="car_buzon" type="text" class="form-control" name="car_buzon" placeholder="Buzón">
                </div>
          </div>
        </div>
			<div class="col-sm-4">
        <label class="checkbox-inline">
          <input type="checkbox" id='cli_activo' value="">Inactivo
        </label>
      </div>
			<div id="status" class = 'col-xs-8'>
      <button id="searchcli" type="button" class="btn btn-default">
        <span class="glyphicon glyphicon-search"></span> Buscar
      </button>
      <button type="submit" id="insertcli" class="btn btn-success">Dar de alta</button>
      </div>
			</div>
        </div>
      </div>
    </div>
  </div>
  <hr>
<div id="content">
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span>
             <h5>Datos de Clientes</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th class = 'col-xs-1'>Código</th>
        					<th class = 'col-xs-1'>Nombre</th>
        					<th class = 'col-xs-2'>Apellidos</th>
        					<th class = 'col-xs-2'>Email</th>
        					<th class = 'col-xs-1'>Extensión</th>
        					<th class = 'col-xs-1'>Fecha de Alta</th>
        					<th class = 'col-xs-1'>Teléfono</th>
        					<th class = 'col-xs-1'>Buzón</th>
                  <th class = 'col-xs-1'>Editar</th>
                </tr>
              </thead>
			             <tbody id = 'searchcontent'>
		              </tbody>
            </table>
          </div>
        </div>
        <div class="widget-box">
      </div>
    </div>
  </div>
</div>
</div>
<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Detalle de cliente</h4>
        </div>
        <div class="modal-body">
          <p>
            <div>
              <!---Código Cliente -->
              <div class="input-group col-xs-">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input id="cli_cododc" type="text" class="form-control" name="cli_cod" placeholder="Código" readonly>
              </div>
              <!---Nombre -->
              <div class="input-group col-xs-">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input id="cli_nomodc" type="text" class="form-control" name="cli_nom" placeholder="Nombre">
              <!---Apellidos -->
              <div class="input-group col-xs-">
              </div>
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input id="cli_surodc" type="text" class="form-control" name="cli_sur" placeholder="Apellido">
              </div>
              <!---Email -->
              <div class="input-group col-xs-">
              <span class="input-group-addon"><i class="glyphicon glyphicon-inbox"></i></span>
              <input id="cli_emailodc" type="text" class="form-control" name="cli_email" placeholder="Email">
              </div>
              <!---Fecha de alta -->
              <div class="input-group col-xs-">
              <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
              <input id="cli_fecha_altaodc" type="text" class="form-control" name="cli_fecha_alta" placeholder="Fecha de alta">
              </div>
              <!---TLF -->
              <div class="input-group col-xs-">
              <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
              <input id="car_tlfodc" type="text" class="form-control" name="car_tlf" placeholder="Tlf">
              </div>
              <!---Extensión -->
              <div class="input-group col-xs-">
              <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i></span>
              <input id="car_extodc" type="text" class="form-control" name="car_ext" placeholder="Extension">
              </div>
              <!---Buzón -->
              <div class="input-group col-xs-">
              <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
              <input id="car_buzonodc" type="text" class="form-control" name="car_buzon" placeholder="Buzon">
              </div>
              <div class="form-check">
                <label class="form-check-label">
                  <input type="checkbox" class="form-check-input">
                  Activo
                </label>
              </div>
            </div>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type='button' id="editcustom"class='btn btn-info btn-sm' data-toggle='modal' data-target='#myModal'>Editar</button>
        </div>
      </div>

    </div>
  </div>
</div>
<?php include('V_footer.php') ?>
