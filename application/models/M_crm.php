<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_crm extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	function index(){
		echo ("Estas en M_crm");
	}
	function buscarcliOBSOLETA($cli_data,$car_data){
			$busqueda = $this->db->query(
			"SELECT *
			FROM clientes
			left join clientes_caracteristicas
			on cli_cod = car_codcli
			WHERE cli_cod like '%".$cli_data['cli_cod']."%'
			or cli_email like '%".$cli_data['cli_email']."%'
			or cli_fecha_alta > '".$cli_data['cli_fecha_alta']."'
			or cli_nom like '%".$cli_data['cli_cod']."%'
			or cli_sur like '%".$cli_data['cli_cod']."%'
			or car_ext like '%".$car_data['car_ext']."%'
			or car_tlf like '%".$car_data['car_tlf']."%'
			limit 1;");

			//"Motor de búsqueda
			if($cli_data['cli_cod']<>'')
			{
			$busqueda = "SELECT *
			FROM clientes
			left join clientes_caracteristicas
			on cli_cod = car_codcli
			WHERE cli_cod = '".$cli_data['cli_cod']."'";
			}
			$filas = $busqueda->num_rows();
			if($filas>0)
			{
				foreach ($busqueda->result_array() as $row){
					$res = array (
					'cli_cod'  => $row['cli_cod'],
					'cli_nom' => $row['cli_nom'],
					'cli_sur' => $row['cli_sur'],
					'cli_email' => $row['cli_email'],
					'cli_activo' => $row['cli_activo'],
					'cli_fecha_alta' => $row['cli_fecha_alta'],
					'car_tlf'  => $row['car_tlf'],
					'car_ext' => $row['car_ext']
					);
				}
					echo "Código: ".$row['cli_cod']."<br>";
					echo "Nombre: ".$row['cli_nom']."<br>";
					echo "Telf: ".$row['car_tlf']."<br>";
					echo "Emai: ".$res['cli_email']."<br>";
				if($this->askactivo($res['cli_activo'])){
					return $res;
				}else{

				}
			}else{
				return false;
				//Pregunta ¿Activar?
				//$this->editactivo($res['cli_cod']);
			}
	}
	function askactivo($cli_activo){
		if($cli_activo==1)
		{
		}else{
			echo "Inactivo";
			return false;
		}
	}
	function editactivo($cli_cod){
		$filas = $mysqli->query("UPDATE clientes
								set cli_activo = '1'
								where cli.codcli = '".$cli_cod."'");
		$exito = $filas->num_rows;
		if ($exito>0)
		{
			return true;
			echo "Éxito";
		}else{
			echo "Hubo un error en la activación de cliente";
		}
	}
	function newcustom($cli_data){
			$insertcli = $this->db->insert('clientes',$cli_data);
			$insertcar = $this->db->insert('clientes_caracteristicas',$car_data);
			$filascli = $insertcli->num_rows();
			$filascar = $insertcar->num_rows();
			if($filascli>0)
			{
				if($filascar>0){
					echo "Exito";
					return true;
				}else{
					echo "Error insertado características";
				}
			}else{
				echo "Error en la inserción de datos del cliente";
			}
	}
	function getcli_cod($cli_cod){
		$query = $this->db->get_where('clientes',array('cli_cod' => $cli_cod));
		$filas = $query->num_rows();
		if($filas>0){
			return true;
		}else{
			return false;
		}
	}
	function getdata($busqueda){
		foreach ($busqueda->result() as $row){
					$res = array (
						'cli_cod'  => $row->cli_cod,
						'cli_nom' => $row->cli_nom,
						'cli_sur' => $row->cli_sur,
						'cli_email' => $row->cli_email,
						'cli_activo' => $row->cli_activo,
						'cli_fecha_alta' => $row->cli_fecha_alta,
						'car_tlf'  => $row->car_tlf,
						'car_ext' => $row->car_ext,
						'car_buzon' => $row->car_buzon
					);
		}
		return $res;
		/*if($this->askactivo($res['cli_activo'])){
			return $res;
		}else{
			//Inactivo-Editar Activo?
			//$this->editactivo($res['cli_cod']);
		}*/
	}
	function buscarcli($cli_data,$car_data){
			//Construcción de la consulta
			$this->db->select('*');
			$this->db->from('clientes');
			$this->db->join('clientes_caracteristicas', 'clientes_caracteristicas.car_codcli = clientes.cli_cod', 'left');

			//Condicionales
			if($cli_data['cli_cod'] <> ''){
				$this->db->where('cli_cod', $cli_data['cli_cod']);
				$busqueda = $this->db->get();
				if($busqueda->num_rows()>0){
					$res=$this->getdata($busqueda);
					return $res;
				}else{
					return false;
				}
			}
			else if($cli_data['cli_nom'] <> ''){
				$this->db->like('cli_nom', $cli_data['cli_nom']);
				$busqueda = $this->db->get();
				if($busqueda->num_rows()>0){
					$res=$this->getdata($busqueda);
					return $res;
				}else{
					return false;
				}
			}
			else if($cli_data['cli_sur'] <> ''){
				$this->db->like('cli_sur', $cli_data['cli_sur']);
				$busqueda = $this->db->get();
				if($busqueda->num_rows()>0){
					$res=$this->getdata($busqueda);
					return $res;
				}else{
					return false;
				}
			}
			else if($cli_data['cli_email'] <> ''){
				$this->db->like('cli_email', $cli_data['cli_email']);
				$busqueda = $this->db->get();
				if($busqueda->num_rows()>0){
					$res=$this->getdata($busqueda);
					return $res;
				}else{
					return false;
				}
			}
			else if($car_data['car_tlf'] <> ''){
				$this->db->like('car_tlf', $car_data['car_tlf']);
				$busqueda = $this->db->get();
				if($busqueda->num_rows()>0){
					$res=$this->getdata($busqueda);
					return $res;
				}else{
					return false;
				}
			}
			else if($car_data['car_ext'] <> ''){
				$this->db->where('car_ext', $car_data['car_ext']);
				$busqueda = $this->db->get();
				if($busqueda->num_rows()>0){
					$res=$this->getdata($busqueda);
					return $res;
				}else{
						return false;
				}
			}
			else if($cli_data['cli_fecha_alta'] <> ''){
				$this->db->where('cli_fecha_alta', $cli_data['cli_fecha_alta']);
				$busqueda = $this->db->get();
				if($busqueda->num_rows()>0){
					$res=$this->getdata($busqueda);
					return $res;
				}else{
					return false;
				}
			}
			else if($car_data['car_buzon'] <> ''){
				$this->db->where('car_buzon', $car_data['car_buzon']);
				$busqueda = $this->db->get();
				if($busqueda->num_rows()>0){
					$res=$this->getdata($busqueda);
					return $res;
				}else{
					return false;
				}
			}
	}
	function insertcli($cli_data,$car_data){
			$this->db->insert('clientes',$cli_data);
			$this->db->insert('clientes_caracteristicas',$car_data);
	}
	function getultimoid($cli_data){
			$this->db->select('*');
			$this->db->from('clientes');
			$this->db->join('clientes_caracteristicas', 'clientes_caracteristicas.car_codcli = clientes.cli_cod', 'left');
			$this->db->where('cli_cod', $cli_data['cli_cod']);
			$this->db->order_by("cli_cod","desc");
			$id=$this->db->get();
			if($query->num_rows()>0){
				$res=$this->getdata($id);
				return $res;
			}

	}
	function editcli($cli_data,$car_data){
		$this->db->set('cli.cli_nom', $cli_data['cli_nom']);
		$this->db->set('cli.cli_sur', $cli_data['cli_sur']);
		$this->db->set('cli.cli_email', $cli_data['cli_email']);
		$this->db->set('cli.cli_fecha_alta', $cli_data['cli_fecha_alta']);
		$this->db->set('car.car_tlf', $car_data['car_tlf']);
		$this->db->set('car.car_ext ', $car_data['car_ext']);
		$this->db->set('car.car_buzon ', $car_data['car_buzon']);
		$this->db->where('cli.cli_cod', $cli_data['cli_cod']);
		$this->db->where('cli.cli_cod = car.car_codcli');
		$this->db->update('clientes as cli ,clientes_caracteristicas as car');
	}
}
